.. hydra documentation master file, created by
   sphinx-quickstart on Wed Aug 25 11:01:43 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Добро пожаловать в документацию АСР Гидра!
==========================================

.. toctree::
   :maxdepth: 2
   :caption: Содержание:

   Add_subject_account
   Print_forms
   One_time_services
   Installment
   Rent
   Add_static


=========================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`